var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM games;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(game_id, callback) {
    //var query = 'SELECT * from games ' +
     //   'left join teams on teams.team_id = games.home_team ' +
      //    'left join teams on teams.team_id = games.away_team ' +
      //  'left join arenas on arena.team_id = games.location ' +
      //  'where game_id = ?';


    var query = 'SELECT arena_name, date, th.team_name, ta.team_name, home_points, away_points, attendance from games ' +
    'left join teams th on th.team_id = games.home_team ' +
    'left join teams ta on ta.team_id = games.away_team ' +
    'left join arenas on arenas.arena_id = games.location ' +
    'where game_id = ?;';

    var queryData = [game_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.insert = function(params, callback) {

    var myQry = 'INSERT INTO games (location, date, home_team, away_team, home_points, away_points, attendance) VALUES (?, ?, ?, ?, ?, ?, ?)';
    var queryData = [params.location, params.date, params.home_team, params.away_team, params.home_points, params.away_points, params.attendance];

    connection.query(myQry, queryData, callback);

};





exports.update = function(params, callback) {
    var query = 'UPDATE games SET location = ?, date = ?, home_team = ?, away_team = ?, home_points = ?, away_points = ?, attendance = ? WHERE game_id = ?';
    var queryData = [params.location, params.date, params.home_team, params.away_team, params.home_points, params.away_points, params.attendance, params.game_id];
    connection.query(query, queryData, callback);
};














exports.delete = function(game_id, callback) {
    var query = 'DELETE FROM games WHERE game_id = ?';
    var queryData = [game_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};



/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS company_getinfo;

 DELIMITER //
 CREATE PROCEDURE company_getinfo (_company_id int)
 BEGIN

 SELECT * FROM company WHERE company_id = _company_id;

 SELECT a.*, s.company_id FROM address a
 LEFT JOIN company_address s on s.address_id = a.address_id AND company_id = _company_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL company_getinfo (4);

 */

exports.edit = function(game_id, callback) {
    var query = 'CALL games_getinfo(?)';
    var queryData = [game_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};