var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM arenas;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(arena_id, callback) {
    var query = 'SELECT * from arenas where arena_id = ?';
    var queryData = [arena_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.insert = function(params, callback) {

    var myQry = 'INSERT INTO arenas (arena_name, street, city, state, zip, phone_number) VALUES (?, ?, ?, ?, ?, ?)';
    var queryData = [params.arena_name, params.street, params.city, params.state, params.zip, params.phone_number];

    connection.query(myQry, queryData, callback);

};





exports.update = function(params, callback) {
    var query = 'UPDATE players SET arena_name = ?, street = ?, city = ?, state = ?, zip = ?, phone_number = ? WHERE arena_id = ?';
    var queryData = [params.arena_name, params.street, params.city, params.state, params.zip, params.phone_number, params.arena_id];
    connection.query(query, queryData, callback);
};














exports.delete = function(arena_id, callback) {
    var query = 'DELETE FROM arenas WHERE arena_id = ?';
    var queryData = [arena_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};



/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS company_getinfo;

 DELIMITER //
 CREATE PROCEDURE company_getinfo (_company_id int)
 BEGIN

 SELECT * FROM company WHERE company_id = _company_id;

 SELECT a.*, s.company_id FROM address a
 LEFT JOIN company_address s on s.address_id = a.address_id AND company_id = _company_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL company_getinfo (4);

 */

exports.edit = function(arena_id, callback) {
    var query = 'CALL arenas_getinfo(?)';
    var queryData = [arena_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};