var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM teams;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(team_id, callback) {
    var query = 'SELECT * from teams where team_id = ?';
    var queryData = [team_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.insert = function(params, callback) {

    var myQry = 'INSERT INTO teams (team_name, points, wins, losses) VALUES (?, ?, ?, ?)';
    var queryData = [params.team_name, params.points, params.wins, params.losses];
    
    connection.query(myQry, queryData, callback);

};





exports.update = function(params, callback) {
    var query = 'UPDATE teams SET team_name = ?, points = ?, wins = ?, losses = ? WHERE team_id = ?';
    var queryData = [params.team_name, params.points, params.wins, params.losses, params.team_id];
    
    /*var query = 'UPDATE teams SET team_name = ? WHERE team_id = ?';
    var queryData = [params.team_name, params.team_id];
    
    var query = 'UPDATE teams SET points = ? WHERE team_id = ?';
    var queryData = [params.points, params.team_id];

    var query = 'UPDATE teams SET wins = ? WHERE team_id = ?';
    var queryData = [params.wins, params.team_id];

    var query = 'UPDATE teams SET losses = ? WHERE team_id = ?';
    var queryData = [params.losses, params.team_id];
*/
    connection.query(query, queryData, callback);
};














exports.delete = function(team_id, callback) {
    var query = 'DELETE FROM teams WHERE team_id = ?';
    var queryData = [team_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};



/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS company_getinfo;

 DELIMITER //
 CREATE PROCEDURE company_getinfo (_company_id int)
 BEGIN

 SELECT * FROM company WHERE company_id = _company_id;

 SELECT a.*, s.company_id FROM address a
 LEFT JOIN company_address s on s.address_id = a.address_id AND company_id = _company_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL company_getinfo (4);

 */

exports.edit = function(team_id, callback) {
    var query = 'CALL teams_getinfo(?)';
    var queryData = [team_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};