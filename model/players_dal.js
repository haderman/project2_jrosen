var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM players;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(player_id, callback) {
    var query = 'SELECT * from players ' +
        'left join teams on teams.team_id = players.plays_for ' +
        'where player_id = ?';
    var queryData = [player_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.insert = function(params, callback) {

    var myQry = 'INSERT INTO players (player_name, plays_for, number, position) VALUES (?, ?, ?, ?)';
    var queryData = [params.player_name, params.plays_for, params.number, params.position];

    connection.query(myQry, queryData, callback);

};





exports.update = function(params, callback) {
    var query = 'UPDATE players SET player_name = ?, plays_for = ?, number = ?, position = ? WHERE player_id = ?';
    var queryData = [params.player_name, params.plays_for, params.number, params.position, params.player_id];
    connection.query(query, queryData, callback);
};














exports.delete = function(player_id, callback) {
    var query = 'DELETE FROM players WHERE player_id = ?';
    var queryData = [player_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};



/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS company_getinfo;

 DELIMITER //
 CREATE PROCEDURE company_getinfo (_company_id int)
 BEGIN

 SELECT * FROM company WHERE company_id = _company_id;

 SELECT a.*, s.company_id FROM address a
 LEFT JOIN company_address s on s.address_id = a.address_id AND company_id = _company_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL company_getinfo (4);

 */

exports.edit = function(player_id, callback) {
    var query = 'CALL players_getinfo(?)';
    var queryData = [player_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};