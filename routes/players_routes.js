var express = require('express');
var router = express.Router();
var players_dal = require('../model/players_dal');
var teams_dal = require('../model/teams_dal');


// View All players
router.get('/all', function(req, res) {
    players_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('players/playersViewAll', { 'result':result });
        }
    });

});

// View the player for the given id
router.get('/', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        players_dal.getById(req.query.player_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('players/playersViewById', {'result': result});
            }
        });
    }
});

// Return the add a new player form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    teams_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('players/playersAdd', {'teams': result});
        }
    });
});

// View the player for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.player_name == null) {
        res.send('Player Name must be provided.');
    }
    else if(req.query.plays_for == null) {
        res.send('The team played for must be provided.');
    }
    else if(req.query.number == null) {
        res.send('Jersey number must be provided.');
    }
    else if(req.query.position == null) {
        res.send('Player position must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        players_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/players/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.player_id == null) {
        res.send('A player id is required');
    }
    else {
        players_dal.edit(req.query.player_id, function(err, result){
            res.render('players/playersUpdate', {players: result[0][0], teams: result[1], number: result[2], position: result[3]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.player_id == null) {
        res.send('A player id is required');
    }
    else {
        players_dal.getById(req.query.player_id, function(err, players){
            teams_dal.getAll(function(err, teams) {
                res.render('players/playersUpdate', {player_name: players[0], teams: teams, number: number, position: position});
            });
        });
    }

});

router.get('/update', function(req, res) {
    players_dal.update(req.query, function(err, result){
        res.redirect(302, '/players/all');
    });
});

// Delete a team for the given team_id
router.get('/delete', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        players_dal.delete(req.query.player_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/players/all');
            }
        });
    }
});





module.exports = router;
