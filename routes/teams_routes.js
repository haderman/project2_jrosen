var express = require('express');
var router = express.Router();
var teams_dal = require('../model/teams_dal');
// var address_dal = require('../model/address_dal');


// View All teams
router.get('/all', function(req, res) {
    teams_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('teams/teamsViewAll', { 'result':result });
        }
    });

});

// View the teams for the given id
router.get('/', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        teams_dal.getById(req.query.team_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('teams/teamsViewById', {'result': result});
            }
        });
    }
});

// Return the add a new team form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    teams_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('teams/teamsAdd', {'address': result});
        }
    });
});

// View the team for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.team_name == null) {
        res.send('Team Name must be provided.');
    }
    else if(req.query.points == null) {
        res.send('Number of points must be provided.');
    }
    else if(req.query.wins == null) {
            res.send('Number of wins must be provided.');
    }
    else if(req.query.losses == null) {
        res.send('Number of losses must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        teams_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/teams/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.team_id == null) {
        res.send('A team id is required');
    }
    else {
        teams_dal.edit(req.query.team_id, function(err, result){
            res.render('teams/teamsUpdate', {teams: result[0][0], points: result[1], wins: result[2], losses: result[3]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.team_id == null) {
        res.send('A team id is required');
    }
    else {
        teams_dal.getById(req.query.team_id, function(err, teams){
                res.render('teams/teamsUpdate', {teams: result[0][0], points: result[1], wins: result[2], losses: result[3]});            });
        
    }

});

router.get('/update', function(req, res) {
    teams_dal.update(req.query, function(err, result){
        res.redirect(302, '/teams/all');
    });
});

// Delete a team for the given team_id
router.get('/delete', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        teams_dal.delete(req.query.team_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/teams/all');
            }
        });
    }
});





module.exports = router;
