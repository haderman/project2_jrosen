var express = require('express');
var router = express.Router();
var games_dal = require('../model/games_dal');
var teams_dal = require('../model/teams_dal');
var arenas_dal = require('../model/arenas_dal');

// View All games
router.get('/all', function(req, res) {
    games_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('games/gamesViewAll', { 'result':result });
        }
    });

});

// View the game for the given id
router.get('/', function(req, res){
    if(req.query.game_id == null) {
        res.send('game_id is null');
    }
    else {
        games_dal.getById(req.query.game_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('games/gamesViewById', {'result': result});
            }
        });
    }
});

// Return the add a new game form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    teams_dal.getAll(function(err,result) {
        
        if (err) {
            res.send(err);
        }
        else {
            res.render('games/gamesAdd', {'teams': result});
        }
    });
});

// View the game for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.location == null) {
        res.send('Arena must be provided.');
    }
    else if(req.query.date == null) {
        res.send('The date for must be provided.');
    }
    else if(req.query.home_team == null) {
        res.send('Home team must be provided.');
    }
    else if(req.query.away_team == null) {
        res.send('Away team must be provided.');
    }
    else if(req.query.home_points == null) {
        res.send('Home points must be provided.');
    }
    else if(req.query.away_points == null) {
        res.send('Away points must be provided.');
    }
    else if(req.query.attendance == null) {
        res.send('attendance must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        games_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/games/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.game_id == null) {
        res.send('A game id is required');
    }
    else {
        games_dal.edit(req.query.game_id, function(err, result){
            res.render('games/gamesUpdate', {games: result[0][0], date: result[1], teams: result[2], teams: result[3], home_points: result[4], away_points: result[5], attendance: result[6]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.game_id == null) {
        res.send('A game id is required');
    }
    else {
        games_dal.getById(req.query.game_id, function(err, games){
            teams_dal.getAll(function(err, teams) {
                res.render('games/gamesUpdate', {games: result[0][0], date: result[1], teams: result[2], teams: result[3], home_points: result[4], away_points: result[5], attendance: result[6]});
            })
        });
    }

});

router.get('/update', function(req, res) {
    games_dal.update(req.query, function(err, result){
        res.redirect(302, '/games/all');
    });
});

// Delete a team for the given team_id
router.get('/delete', function(req, res){
    if(req.query.game_id == null) {
        res.send('game_id is null');
    }
    else {
        games_dal.delete(req.query.game_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/games/all');
            }
        });
    }
});





module.exports = router;
