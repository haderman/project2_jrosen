var express = require('express');
var router = express.Router();
var arenas_dal = require('../model/arenas_dal');


// View All arenas
router.get('/all', function(req, res) {
    arenas_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('arenas/arenasViewAll', { 'result':result });
        }
    });

});

// View the arenas for the given id
router.get('/', function(req, res){
    if(req.query.arena_id == null) {
        res.send('arena_id is null');
    }
    else {
        arenas_dal.getById(req.query.arena_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('arenas/arenasViewById', {'result': result});
            }
        });
    }
});

// Return the add a new arena form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    arenas_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('arenas/arenasAdd', {'address': result});
        }
    });
});

// View the arena for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.arena_name == null) {
        res.send('arena Name must be provided.');
    }
    else if(req.query.street == null) {
        res.send('Street Address must be provided.');
    }
    else if(req.query.city == null) {
        res.send('City must be provided.');
    }
    else if(req.query.state == null) {
        res.send('State must be provided.');
    }
    else if(req.query.zip == null) {
        res.send('Zip Code must be provided.');
    }
    else if(req.query.phone_number == null) {
        res.send('Phone Number must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        arenas_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/arenas/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.arena_id == null) {
        res.send('A arena id is required');
    }
    else {
        arenas_dal.edit(req.query.arena_id, function(err, result){
            res.render('arenas/arenasUpdate', {arenas: result[0][0], street: result[1], city: result[2], state: result[3], zip: result[4], phone_number: result[5]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.arena_id == null) {
        res.send('A arena id is required');
    }
    else {
        arenas_dal.getById(req.query.arena_id, function(err, arenas){
            res.render('arenas/arenasUpdate', {arenas: result[0][0], street: result[1], city: result[2], state: result[3], zip: result[4], phone_number: result[5]});
        });

    }

});

router.get('/update', function(req, res) {
    arenas_dal.update(req.query, function(err, result){
        res.redirect(302, '/arenas/all');
    });
});

// Delete a arena for the given arena_id
router.get('/delete', function(req, res){
    if(req.query.arena_id == null) {
        res.send('arena_id is null');
    }
    else {
        arenas_dal.delete(req.query.arena_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/arenas/all');
            }
        });
    }
});





module.exports = router;
